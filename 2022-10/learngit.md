> ❗*[廖雪峰Git教程](https://www.liaoxuefeng.com/wiki/896043488029600)*

# 基础命令

## 推送本地仓库

```shell
mkdir learngit
cd learngit
git init
# 将文件添加到暂存区
git add .
# 将暂存区的改动提交到本地仓库
git commit -m '注释'
git remote add origin git@gitee.com:who/learngit.git
# 修改远程仓库名称
git remote rename old new_name
# 推送分支
git push -u origin master
# 删除本地仓库文件
git rm -r dirname
git commit -m 'rm dirname'
# 删除远程仓库（关联）
git remote rm origin 
```

## ssh-keys

```sheel
# windows下生成公钥利用git-bash工具
# run git-bash
ssh-keygen -t rsa -C "git@gitee.com:who/learngit.git"

	# -t 类型，默认rsa
	# -f 保存密钥文件名称
	# -C 注释
# 测试key
ssh -T git@gitee.com
```

# 分支管理

```shell
git branch
# 创建分支
git switch -c master
git checkout -b branch
# 切换分支
git switch branch
git checkout branch_name
# 删除分支
git checkout -d branch_name
git branch -d branch_name
# 删除远程分支
git push origin --delete branch_name
# 合并分支
# 将分支合并到当前分支
git merge branch_name
```

## 代码冲突

## feature功能分支

```shell
git checkout -b feature_pdf
vi pdf.php
git add pdf.php
git commit -m 'add feature_pdf'
git switch dev
... # 准备合并
# 删除分支
git branch -d feature_pdf
# 强行删除
git branch -D feature_pdf
```

## 多人协作问题

**协作流程**

1. 首先，可以试图用`git push origin <branch-name>`推送自己的修改；
2. 如果推送失败，则因为远程分支比你的本地更新，需要先用`git pull`试图合并；
3. 如果合并有冲突，则解决冲突，并在本地提交；
4. 没有冲突或者解决掉冲突后，再用`git push origin <branch-name>`推送就能成功！
5. 没有冲突或者解决掉冲突后，再用`git push origin <branch-name>`推送就能成功！

> 🔔 如果`git pull`提示`no tracking information`，则说明本地分支和远程分支的链接关系没有创建，
>
> 用命令`git branch --set-upstream-to <branch-name> origin/<branch-name>`。


```shell
# 抓取分支
git clone git@gitee.com:who/learngit.git
# 本地仓库创建dev分支
git checkout -b dev origin/dev
... # 修改操作
git add env.txt
git commit -m ''
git push origin dev
# 发生冲突
git pull
# 指定本地dev分支与远程origin/dev的链接（关联）
git branch setup-stream-to=origin/dev dev
# 再次pull
git pull
git commit -m 'fix conflict'
git push origin dev
```

# 标签管理

> ❗ 标签总是和某个commit挂钩。如果这个commit既出现在master分支，又出现在dev分支，那么在这两个分支上都可以看到这个标签。

```shell
# create
git tag <tagname> <commit_id>
git tag -a <tagname> -m 'msg' <commit_id>
# show
git show tagname
# delete
git tag -d tagname
# 推送到远程仓库
git push origin tagname
# 推送所有tag到远程仓库
git push origin --tags
# 删除本地仓库tag
git tag -d tagname
# 从远程仓库删除tag
git push origin :refs/tags/tagname
```

# 代码托管平台

## GitHub

```ascii
┌─ GitHub ────────────────────────────────────┐
│                                             │
│ ┌─────────────────┐     ┌─────────────────┐ │
│ │ twbs/bootstrap  │────>│  my/bootstrap   │ │
│ └─────────────────┘     └─────────────────┘ │
│                                  ▲          │
└──────────────────────────────────┼──────────┘
                                   ▼
                          ┌─────────────────┐
                          │ local/bootstrap │
                          └─────────────────┘
```

- 在GitHub上，可以任意Fork开源仓库；
- 自己拥有Fork后的仓库的读写权限；
- 可以推送pull request给官方仓库来贡献代码;

## Gitee

```ascii
┌─────────┐ ┌─────────┐
│ GitHub  │ │  Gitee  │
└─────────┘ └─────────┘
     ▲           ▲
     └─────┬─────┘
           │
    ┌─────────────┐
    │ Local Repo  │
    └─────────────┘
```

> 🔔本地仓库可以关联多个远程仓库
