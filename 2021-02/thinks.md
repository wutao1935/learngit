## 一点心得
    这两天快速的又过了一遍教程，十分感谢雪峰老师这个教程。但是同时个人
    有点小建议：
* 有些命令已经过时了，git也不推荐了，例如git checkout ...
* 我看评论区，有些地方有些争议，希望雪峰老师能够给一些正确的解释。
* 以上如果老师没有时间去处理，可以将课程开源出来，一同完善。